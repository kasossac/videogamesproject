// Copyright Epic Games, Inc. All Rights Reserved.

#include "VideoGamesProjectGameMode.h"
#include "VideoGamesProjectCharacter.h"
#include "UObject/ConstructorHelpers.h"

AVideoGamesProjectGameMode::AVideoGamesProjectGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
