// Copyright Epic Games, Inc. All Rights Reserved.

#include "VideoGamesProject.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, VideoGamesProject, "VideoGamesProject" );
 