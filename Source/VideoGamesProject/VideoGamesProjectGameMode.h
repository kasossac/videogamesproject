// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "VideoGamesProjectGameMode.generated.h"

UCLASS(minimalapi)
class AVideoGamesProjectGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AVideoGamesProjectGameMode();
};



