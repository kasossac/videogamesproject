// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VIDEOGAMESPROJECT_VideoGamesProjectCharacter_generated_h
#error "VideoGamesProjectCharacter.generated.h already included, missing '#pragma once' in VideoGamesProjectCharacter.h"
#endif
#define VIDEOGAMESPROJECT_VideoGamesProjectCharacter_generated_h

#define VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectCharacter_h_12_SPARSE_DATA
#define VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectCharacter_h_12_RPC_WRAPPERS
#define VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectCharacter_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAVideoGamesProjectCharacter(); \
	friend struct Z_Construct_UClass_AVideoGamesProjectCharacter_Statics; \
public: \
	DECLARE_CLASS(AVideoGamesProjectCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/VideoGamesProject"), NO_API) \
	DECLARE_SERIALIZER(AVideoGamesProjectCharacter)


#define VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectCharacter_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAVideoGamesProjectCharacter(); \
	friend struct Z_Construct_UClass_AVideoGamesProjectCharacter_Statics; \
public: \
	DECLARE_CLASS(AVideoGamesProjectCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/VideoGamesProject"), NO_API) \
	DECLARE_SERIALIZER(AVideoGamesProjectCharacter)


#define VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectCharacter_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AVideoGamesProjectCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AVideoGamesProjectCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AVideoGamesProjectCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AVideoGamesProjectCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AVideoGamesProjectCharacter(AVideoGamesProjectCharacter&&); \
	NO_API AVideoGamesProjectCharacter(const AVideoGamesProjectCharacter&); \
public:


#define VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AVideoGamesProjectCharacter(AVideoGamesProjectCharacter&&); \
	NO_API AVideoGamesProjectCharacter(const AVideoGamesProjectCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AVideoGamesProjectCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AVideoGamesProjectCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AVideoGamesProjectCharacter)


#define VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(AVideoGamesProjectCharacter, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__FollowCamera() { return STRUCT_OFFSET(AVideoGamesProjectCharacter, FollowCamera); }


#define VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectCharacter_h_9_PROLOG
#define VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectCharacter_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectCharacter_h_12_SPARSE_DATA \
	VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectCharacter_h_12_RPC_WRAPPERS \
	VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectCharacter_h_12_INCLASS \
	VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectCharacter_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectCharacter_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectCharacter_h_12_SPARSE_DATA \
	VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectCharacter_h_12_INCLASS_NO_PURE_DECLS \
	VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VIDEOGAMESPROJECT_API UClass* StaticClass<class AVideoGamesProjectCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
