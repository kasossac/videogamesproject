// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VIDEOGAMESPROJECT_VideoGamesProjectGameMode_generated_h
#error "VideoGamesProjectGameMode.generated.h already included, missing '#pragma once' in VideoGamesProjectGameMode.h"
#endif
#define VIDEOGAMESPROJECT_VideoGamesProjectGameMode_generated_h

#define VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectGameMode_h_12_SPARSE_DATA
#define VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectGameMode_h_12_RPC_WRAPPERS
#define VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAVideoGamesProjectGameMode(); \
	friend struct Z_Construct_UClass_AVideoGamesProjectGameMode_Statics; \
public: \
	DECLARE_CLASS(AVideoGamesProjectGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/VideoGamesProject"), VIDEOGAMESPROJECT_API) \
	DECLARE_SERIALIZER(AVideoGamesProjectGameMode)


#define VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAVideoGamesProjectGameMode(); \
	friend struct Z_Construct_UClass_AVideoGamesProjectGameMode_Statics; \
public: \
	DECLARE_CLASS(AVideoGamesProjectGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/VideoGamesProject"), VIDEOGAMESPROJECT_API) \
	DECLARE_SERIALIZER(AVideoGamesProjectGameMode)


#define VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	VIDEOGAMESPROJECT_API AVideoGamesProjectGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AVideoGamesProjectGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(VIDEOGAMESPROJECT_API, AVideoGamesProjectGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AVideoGamesProjectGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	VIDEOGAMESPROJECT_API AVideoGamesProjectGameMode(AVideoGamesProjectGameMode&&); \
	VIDEOGAMESPROJECT_API AVideoGamesProjectGameMode(const AVideoGamesProjectGameMode&); \
public:


#define VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	VIDEOGAMESPROJECT_API AVideoGamesProjectGameMode(AVideoGamesProjectGameMode&&); \
	VIDEOGAMESPROJECT_API AVideoGamesProjectGameMode(const AVideoGamesProjectGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(VIDEOGAMESPROJECT_API, AVideoGamesProjectGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AVideoGamesProjectGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AVideoGamesProjectGameMode)


#define VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectGameMode_h_9_PROLOG
#define VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectGameMode_h_12_SPARSE_DATA \
	VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectGameMode_h_12_RPC_WRAPPERS \
	VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectGameMode_h_12_INCLASS \
	VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectGameMode_h_12_SPARSE_DATA \
	VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectGameMode_h_12_INCLASS_NO_PURE_DECLS \
	VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VIDEOGAMESPROJECT_API UClass* StaticClass<class AVideoGamesProjectGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID VideoGamesProject_Source_VideoGamesProject_VideoGamesProjectGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
