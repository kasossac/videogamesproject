// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VideoGamesProject/VideoGamesProjectGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVideoGamesProjectGameMode() {}
// Cross Module References
	VIDEOGAMESPROJECT_API UClass* Z_Construct_UClass_AVideoGamesProjectGameMode_NoRegister();
	VIDEOGAMESPROJECT_API UClass* Z_Construct_UClass_AVideoGamesProjectGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_VideoGamesProject();
// End Cross Module References
	void AVideoGamesProjectGameMode::StaticRegisterNativesAVideoGamesProjectGameMode()
	{
	}
	UClass* Z_Construct_UClass_AVideoGamesProjectGameMode_NoRegister()
	{
		return AVideoGamesProjectGameMode::StaticClass();
	}
	struct Z_Construct_UClass_AVideoGamesProjectGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AVideoGamesProjectGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_VideoGamesProject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVideoGamesProjectGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "VideoGamesProjectGameMode.h" },
		{ "ModuleRelativePath", "VideoGamesProjectGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AVideoGamesProjectGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AVideoGamesProjectGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AVideoGamesProjectGameMode_Statics::ClassParams = {
		&AVideoGamesProjectGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_AVideoGamesProjectGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AVideoGamesProjectGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AVideoGamesProjectGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AVideoGamesProjectGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AVideoGamesProjectGameMode, 1267702579);
	template<> VIDEOGAMESPROJECT_API UClass* StaticClass<AVideoGamesProjectGameMode>()
	{
		return AVideoGamesProjectGameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AVideoGamesProjectGameMode(Z_Construct_UClass_AVideoGamesProjectGameMode, &AVideoGamesProjectGameMode::StaticClass, TEXT("/Script/VideoGamesProject"), TEXT("AVideoGamesProjectGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AVideoGamesProjectGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
